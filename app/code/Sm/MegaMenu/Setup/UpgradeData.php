<?php

namespace Sm\MegaMenu\Setup;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * {@inheritdoc}
     */    
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
            if (version_compare($context->getVersion(), '3.2.1', '<')) {
                $this->registerCustomerCredit($setup);
            }
        $setup->endSetup();
    }

    public function registerCustomerCredit($setup){
        $connection = $setup->getConnection();
        $sm_megamenu_groups_table = $setup->getTable('sm_megamenu_groups');
        $sm_megamenu_items_table = $setup->getTable('sm_megamenu_items');
        // Check if the table already exists
        if ($connection->isTableExists($sm_megamenu_groups_table)) {
            $setup->run("TRUNCATE {$sm_megamenu_groups_table}");
            $setup->run("INSERT INTO {$sm_megamenu_groups_table} (group_id, title, status, content) VALUES (1, 'Main Menu', 1, '')");
        }
        if ($connection->isTableExists($sm_megamenu_items_table)) {
            $data1 = [
//                'items_id' => 1,
                'title' => "Root[Header Menu]",
                'show_title' => 1,
                'description' => "",
                'status' => 1,
                'align' => 1,
                'depth' => 0,
                'group_id' => 1,
                'cols_nb' => 0,
                'target' => 1,
                'type' => 0,
                'content' => "",
                'parent_id' => 0,
                'order_item' => 0,
                'position_item' => 2,
                'priorities' => 0,
                'show_image_product' => 0,
                'show_title_product' => 0,
                'show_rating_product' => 0,
                'show_price_product' => 0,
                'show_title_category' => 0,
                'show_sub_category' => 1,
            ];
            $data2 = [
//                'items_id' => 2,
                'title' => "Home",
                'show_title' => 1,
                'description' => "",
                'status' => 1,
                'align' => 1,
                'depth' => 0,
                'group_id' => 1,
                'cols_nb' => 3,
                'target' => 1,
                'type' => 1,
                'content' => "",
                'parent_id' => 1,
                'order_item' => 2,
                'position_item' => 2,
                'priorities' => 3,
                'show_image_product' => 0,
                'show_title_product' => 0,
                'show_rating_product' => 0,
                'show_price_product' => 0,
                'show_title_category' => 0,
                'show_sub_category' => 1,
            ];

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $model1 = $objectManager->create('Sm\MegaMenu\Model\MenuItems')->setData($data1)->save();
            $model2 = $objectManager->create('Sm\MegaMenu\Model\MenuItems')->setData($data2)->save();
        }
    }
}