var config = {
	map: {
		'*': {
			jquerybootstrap: "Sm_Multe/js/bootstrap/bootstrap.min",
			owlcarousel: "Sm_Multe/js/owl.carousel",
			slickslider: "Sm_Multe/js/slick",
			jqueryfancyboxpack: "Sm_Multe/js/jquery.fancybox.pack",
			jqueryfancyboxmedia: "Sm_Multe/js/jquery.fancybox-media",
			yttheme: "Sm_Multe/js/yttheme"
		}
	}
};